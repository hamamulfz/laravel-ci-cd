<?php

namespace App\Http\Controllers;

use App\EditionList;
use App\TemporaryList;
use Illuminate\Http\Request;

class UploadController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('upload');
    }

    public function uploadPdf(Request $request)
    {
        $this->validate($request, [
            'file' => 'required',
        ]);

        // menyimpan data file yang diupload ke variabel $file

        $files = $request->file('file');

        foreach ($files as $file) {
            // nama file
            // echo 'File Name: ' . $file->getClientOriginalName();
            // echo '<br>';
            $filename = $file->getClientOriginalName();
            $name = substr($filename, 4);
            $title = str_replace(" - ", "", $name);
            $title = str_replace(".pdf", "", $title);
            $title = str_replace("[BOOK]", "", $title);
            $title = str_replace("[CETAK]", "", $title);

            $upload = new TemporaryList();
            $upload->year = substr($filename, 0, 2);
            $upload->edition = substr($filename, 2, 3);
            $upload->year_edition = substr($filename, 0, 4);
            $upload->title = $title;
            $upload->pdf_url = "pdf/" . $filename;
            $upload->save();


            // ekstensi file
            // echo 'File Extension: ' . $file->getClientOriginalExtension();
            // echo '<br>';

            // real path
            // echo 'File Real Path: ' . $file->getRealPath();
            // echo '<br>';

            // ukuran file
            // echo 'File Size: ' . $file->getSize();
            // echo '<br>';

            // tipe mime
            // echo 'File Mime Type: ' . $file->getMimeType();

            // isi dengan nama folder tempat kemana file diupload
            $tujuan_upload = 'pdf';

            // upload file
            $file->move($tujuan_upload, $file->getClientOriginalName());
            
        }
        // die();
        return view('upload', ['file']);
    }
}
