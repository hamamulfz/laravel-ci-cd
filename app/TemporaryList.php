<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemporaryList extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'temporary_edition';
}
