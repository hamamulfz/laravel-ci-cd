@extends('layouts.dashboard')

@section('title', 'Buletin Tauhid')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"> HOME
                            {{-- <button type="button" class="btn btn-success mr-5 open_modal" data-toggle="modal" data-target="#modalMd">
                                <i class="fa fa-plus-circle"></i> Upload PDF
                            </button> --}}
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row" style="margin-top:10px">
                            <div class="col-lg-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr> 
                                    <th>Tahun</th>
                                    <th>Edisi</th>
                                    <th>Judul</th>
                                    <th>Nama File</th>
                                    
                                    <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{ route('upload-pdf') }}" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upload PDF</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                        {{ csrf_field() }}
                        <label>Choose PDF File</label>
                        <div class="form-group">
                            <input type="file" name="file[]" accept="application/pdf" required="required" multiple>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection


@section('script')
<script>
   $(document).on('click','.open_modal',function(){
       console.log('clicked');
        $('#myModal').modal('show');
    });
</script>
@endsection
