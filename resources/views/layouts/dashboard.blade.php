<!DOCTYPE html>
<html lang="en">
<head>
  @include('layouts.admin.head')
  @yield('styling')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    @include('layouts.admin.navbar')
    @include('layouts.admin.right-navbar')
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->

  @include('layouts.admin.sidebar')
  <!-- /.sidebar -->

  @yield('content')

  @include('layouts.admin.footer')



  @include('layouts.admin.script')
  @yield('script')
</body>
</html>
