@extends('layouts.dashboard')

@section('title', 'Buletin Tauhid')

@section('styling')
<style>
.divider{
    width:5px;
    height:auto;
    display:inline-block;
}
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0">Upload New Buletin</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Upload</li>
            </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <button type="button" class="btn btn-success mr-5 open_modal" data-toggle="modal" data-target="#modalMd">
                                <i class="fa fa-plus-circle"></i> Upload PDF
                            </button>
                        </h3>
                    </div>
                    {{-- CARD BODY --}}
                    <div class="card-body">
                        <div class="row" style="margin-top:10px">
                            <div class="col-lg-12">
                                {{-- TABLE --}}
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr> 
                                    <th>Tahun</th>
                                    <th>Edisi</th>
                                    <th>Judul</th>
                                    <th>Nama File</th>
                                    
                                    <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                </table>
                                {{-- TABLE --}}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer clearfix">
                        <button  class="btn btn-md btn-success float-right">
                            {{-- <i class="fa fa-exclamation-triangle"></i>  --}}
                            Confirm
                         </button>
                        
                        <button style="margin-right: 20px;" class="btn btn-md btn-danger float-right">
                            {{-- <i class="fa fa-exclamation-triangle"></i>  --}}
                            Cancel 
                        </button>
                        {{-- <span class="float-left">{{ $wos->links() }}</span> --}}
                        {{-- @if($wos->count() > 0)
                        @endif --}}
                    </div>
                </div>
                {{-- CARD BODY --}}
            </div>
            {{-- CARD --}}
        </div>
    </div>
    {{-- CONTAINER CARD --}}
</div>

<div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form method="post" action="{{ route('upload-pdf') }}" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upload PDF</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                        {{ csrf_field() }}
                        <label>Choose PDF File</label>
                        <div class="form-group">
                            <input type="file" name="file[]" accept="application/pdf" required="required" multiple>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection


@section('script')
<script>
   $(document).on('click','.open_modal',function(){
       console.log('clicked');
        $('#myModal').modal('show');
    });
</script>
@endsection
